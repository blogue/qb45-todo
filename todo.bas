
DECLARE SUB Main ()
DECLARE SUB MainMenu ()
DECLARE SUB CreateTodo ()
DECLARE SUB DeleteTodo ()
DECLARE SUB MarkTodoAsDone ()
DECLARE SUB DisplayTodos ()
DECLARE FUNCTION ValidateAtLeastOneTodo ()

TYPE todotype
  desc AS STRING * 50
  done AS INTEGER
  deleted AS INTEGER
END TYPE

CONST YES% = 1, NO% = 0

CONST MAXTODOS% = 5
DIM SHARED todocount AS INTEGER
todocount = 0

DIM SHARED todos(MAXTODOS%) AS todotype
FOR i% = 1 TO MAXTODOS%
  todos(i%).deleted = YES%
NEXT i%

Main

SUB CreateTodo
  DIM newtodo AS todotype
  newtodo.done = NO%
  newtodo.deleted = NO%

  IF todocount = MAXTODOS% THEN
    PRINT "Impossible d'ajouter plus de " + STR$(MAXTODOS%) + " todos"
    PRINT "Appuyez sur une touche pour continuer"
    DO
    LOOP UNTIL INKEY$ <> ""

    EXIT SUB
  END IF

  PRINT "Creation d'un nouveau todo."
  INPUT "Saisir la description : "; newtodo.desc

  todocount = todocount + 1
  todos(todocount) = newtodo
END SUB

SUB DeleteTodo
  DIM todoindex AS INTEGER
  IF ValidateAtLeastOneTodo = NO% THEN EXIT SUB

  DO
    PRINT "Entre 1 et" + STR$(todocount)
    INPUT "Lequel"; todoindex
    IF todoindex >= 1 AND todoindex <= todocount THEN EXIT DO
  LOOP

  todos(todoindex).deleted = YES%
  todocount = todocount - 1
END SUB

SUB DisplayTodos
  FOR i = 1 TO MAXTODOS%
    DIM currenttodo AS todotype
    DIM donestring AS STRING
    currenttodo = todos(i)

    IF currenttodo.deleted = NO% THEN
      IF currenttodo.done = YES% THEN
        donestring = "Fait"
      ELSE
        donestring = "Pas fait"
      END IF

      PRINT currenttodo.desc + " - " + donestring
    END IF
  NEXT i
END SUB

SUB Main
  DIM userinput AS STRING

  DO
    MainMenu
   
    INPUT "Votre choix? (tapez quit pour quitter)"; userinput
   
    IF userinput = "4" THEN EXIT DO
    IF userinput = "1" THEN CreateTodo
    IF userinput = "2" THEN MarkTodoAsDone
    IF userinput = "3" THEN DeleteTodo
  LOOP
END SUB

SUB MainMenu
  CLS
  PRINT "ToDo App - QBasic 4.5"
  PRINT

  IF todocount > 0 THEN
    DisplayTodos
  ELSE
    PRINT "Aucun todo"
  END IF

  PRINT
  PRINT "1. Creer un todo"
  PRINT "2. Marquer un todo comme fait"
  PRINT "3. Supprimer un todo"
  PRINT "4. Quitter"
  PRINT
END SUB

SUB MarkTodoAsDone
  DIM todoindex AS INTEGER
  IF ValidateAtLeastOneTodo = NO% THEN EXIT SUB

  DO
    PRINT "Entre 1 et" + STR$(todocount)
    INPUT "Lequel"; todoindex
    IF todoindex >= 1 AND todoindex <= todocount THEN EXIT DO
  LOOP

  todos(todoindex).done = YES%
END SUB

FUNCTION ValidateAtLeastOneTodo
  DIM valid AS INTEGER
  valid = YES%
  IF todocount = 0 THEN
    valid = NO%
    PRINT "Aucun todo en memoire..."
    PRINT "Appuyez sur une touche pour continuer"
    DO
    LOOP UNTIL INKEY$ <> ""
  END IF

  ValidateAtLeastOneTodo = valid
END FUNCTION
